#!/usr/bin/env python3

import logging as LOG

try:
    LOG.basicConfig(filename='/var/log/mcloginlog.log', level=LOG.INFO)
except PermissionError:
    LOG.basicConfig(filename='/tmp/mcloginlog.log', level=LOG.INFO)
