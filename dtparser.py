#!/usr/bin/env python3

from datetime import datetime
import logging as LOG
import pytz

LOCAL_TIMEZONE = pytz.timezone('Europe/Berlin')


def parse(date_str):
    try:
        local_time = datetime.strptime(date_str, "%d%b%Y %H:%M:%S.%f")
    except ValueError:
        LOG.error("could not parse %s as a date", date_str)
        local_time = datetime.now()
    return LOCAL_TIMEZONE.localize(local_time).astimezone(pytz.utc)
